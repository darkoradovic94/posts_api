import React, { FC } from "react";
import styles from "./Comment.module.css";

interface Props {
  title?: string;
  comment: string;
  message: string;
  name: string;
}

export const Comment: FC<Props> = ({ title, comment, message, name }) => {
  console.log(`${message} ${name}`);
  return (
    <div className={styles.single_comment}>
      {title && <h1>{title}</h1>}
      <p> {comment}</p>
    </div>
  );
};
