import React, { FC } from "react";
import styles from "./SearchFilter.module.css";

interface Props {
  onChange: (e: any) => void;
  message: string;
  name: string;
}

export const SearchFilter: FC<Props> = ({ onChange, message, name }) => {
  console.log(`${message} ${name}`);
  return (
    <div className={styles.wrap}>
      <div className={styles.search}>
        <input
          type="text"
          className={styles.searchTerm}
          placeholder="Search post by user name or email"
          onChange={onChange}
        />
        <button type="submit" className={styles.searchButton}>
          <i className="fa fa-search"></i>
        </button>
      </div>
    </div>
  );
};
