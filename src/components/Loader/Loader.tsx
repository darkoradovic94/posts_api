import React, { FC } from "react";
import styles from "./Loader.module.css";

export const Loader: FC = () => {
  return (
    <div className={styles.wrap}>
      <div className={styles.lds_dual_ring}></div>
    </div>
  );
};
