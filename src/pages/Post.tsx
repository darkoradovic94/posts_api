import axios from "axios";
import React, { useContext, useEffect, useState } from "react";

import { useParams } from "react-router-dom";
import { Comment } from "../components/Comment";
import { Loader } from "../components/Loader";
import { Context } from "../context";

export const Post = () => {
  const [post, setPost] = useState<any>({});
  const { loading, setLoading } = useContext(Context);
  const { id } = useParams();

  const fetchDataById = () => {
    setLoading(true);
    axios
      .get(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((res) => {
        setPost(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchDataById();
  }, []);
  return (
    <div className="single_post_wrap">
      {loading ? (
        <Loader />
      ) : (
        <Comment
          comment={post.body}
          title={
            post.title &&
            post.title.charAt(0).toUpperCase() + post.title.slice(1)
          }
          message="Hellow from"
          name="Single comment"
        />
      )}
    </div>
  );
};
