import React, { FC } from "react";
import { Link } from "react-router-dom";

export const HomePage: FC = () => {
  return (
    <div className="btn_wrap">
      <Link to="/posts">
        <button className="btn">Go to posts</button>
      </Link>
    </div>
  );
};
