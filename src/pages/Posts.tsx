import React, { FC, useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";

import { Comment } from "../components/Comment";
import { Loader } from "../components/Loader";
import { SearchFilter } from "../components/SearchFilter";
import { Context } from "../context/context";
import { IComments, IPosts, IUsers } from "../models/posts";

interface Props {
  posts: IPosts[];
  comments: IComments[];
  users: IUsers[];
}

export const Posts: FC<Props> = ({ posts, comments, users }) => {
  const [search, setSearch] = useState("");
  const [filteredData, setFilteredData] = useState<any>([]);
  const { loading } = useContext(Context);

  useEffect(() => {
    let filteredData = users;
    if (search !== "") {
      filteredData = filteredData.filter((option: any) => {
        return (
          option.name.toLowerCase().includes(search.toLowerCase()) ||
          option.email.toLowerCase().includes(search.toLowerCase())
        );
      });
    }

    setFilteredData(filteredData);
  }, [search, users]);
  return (
    <>
      <SearchFilter
        onChange={(e) => setSearch(e.target.value)}
        message="Hello from"
        name="Search component"
      />
      <div className="posts_container">
        {loading ? (
          <Loader />
        ) : (
          posts.map((post: IPosts, i: number) => (
            <div key={i}>
              {filteredData.map(
                (item: IUsers) =>
                  post.userId === item.id && (
                    <div key={post.id} className="post_title">
                      <h5>User: {item.name} </h5>
                      <Link to={`/posts/${post.id}`}>
                        <h3>
                          {post.title.charAt(0).toUpperCase() +
                            post.title.slice(1)}
                        </h3>
                      </Link>

                      <div className="comments">
                        <h3>Comments:</h3>
                        {comments
                          .slice(0, 100)
                          .map(
                            (comment: IComments, i: number) =>
                              post.userId === comment.postId && (
                                <Comment
                                  key={i}
                                  comment={comment.body}
                                  message="Hellow from"
                                  name="Comment component"
                                />
                              )
                          )}
                      </div>
                    </div>
                  )
              )}
            </div>
          ))
        )}
      </div>
    </>
  );
};
