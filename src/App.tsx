import React, { useState, useEffect, useContext } from "react";
import { Route, Routes } from "react-router-dom";
import axios from "axios";

import "./App.css";

import { HomePage } from "./pages/HomePage";
import { Post } from "./pages/Post";
import { Context } from "./context/context";
import { Posts } from "./pages/Posts";

const App = () => {
  const [posts, setPosts] = useState<[]>([]);
  const [comments, setComments] = useState<[]>([]);
  const [users, setUsers] = useState<[]>([]);
  const { setLoading } = useContext(Context);

  const urlPosts = "https://jsonplaceholder.typicode.com/posts";
  const urlComments = "https://jsonplaceholder.typicode.com/comments";
  const urlUsers = "https://jsonplaceholder.typicode.com/users";

  const fetchPosts = () => {
    setLoading(true);
    axios
      .get(urlPosts)
      .then((res) => {
        setPosts(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const fetchComments = () => {
    setLoading(true);
    axios
      .get(urlComments)
      .then((res) => {
        setComments(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const fetchUsers = () => {
    setLoading(true);
    axios
      .get(urlUsers)
      .then((res) => {
        setUsers(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchPosts();
    fetchComments();
    fetchUsers();
  }, []);

  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route
          path="/posts"
          element={<Posts posts={posts} comments={comments} users={users} />}
        />
        <Route path="/posts/:id" element={<Post />} />
      </Routes>
    </div>
  );
};

export default App;
