import React, { Dispatch, SetStateAction, useState } from "react";

type ContextProps = {
  loading: boolean;
  setLoading: Dispatch<SetStateAction<boolean>>;
};

let Context: any;
let { Provider } = (Context = React.createContext<Partial<ContextProps>>({}));

let ContextProvider = ({ children }: { children: React.ReactNode }) => {
  const [loading, setLoading] = useState<boolean>(false);

  return <Provider value={{ loading, setLoading }}>{children}</Provider>;
};

export { Context, ContextProvider };
