export interface IPosts{
    body: string
    id: number
    title: string
    userId: number
}

export interface IComments{
    postId: number
    id: number
    name: string
    email: string
    body: string
}

export interface IUsers{
    address: IAddress
    company: ICompany
    email: string
    id: number
    name: string
    phone: string
    username: string
    website: string
}

export interface IAddress{
    city: string
    geo: null
    street: string
    suite: string
    zipcode: string
}

export interface ICompany{
    name: string
    catchPhrase: string
    bs: string
}